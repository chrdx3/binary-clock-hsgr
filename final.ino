#include "Wire.h"
#include "stdio.h"


#define DS3231_I2C_ADDRESS 0x68

//FUNCTIONS

int* dec2bin(int c,int *BinBuffer)
{
   int i = 0;
  
   size_t currentsize = 0;

   for(i = 3; i >= 0; i--)
   {
     if((c & (1 << i)) != 0)
     {
       BinBuffer[currentsize++] = 1;
     }
     else
     {
       BinBuffer[currentsize++] = 0;
     }
   }
   return BinBuffer;
}

// Convert normal decimal numbers to binary coded decimal
byte decToBcd(byte val){
  return( (val/10*16) + (val%10) );
}
// Convert binary coded decimal to normal decimal numbers
byte bcdToDec(byte val){
  return( (val/16*10) + (val%16) );
}






//SETS time from computer to RTC module via serial
void setDS3231time(byte second, byte minute, byte hour, byte dayOfWeek, byte
dayOfMonth, byte month, byte year)
{
  // sets time and date data to DS3231
  Wire.beginTransmission(DS3231_I2C_ADDRESS);
  Wire.write(0); // set next input to start at the seconds register
  Wire.write(decToBcd(second)); // set seconds
  Wire.write(decToBcd(minute)); // set minutes
  Wire.write(decToBcd(hour)); // set hours
  Wire.write(decToBcd(dayOfWeek)); // set day of week (1=Sunday, 7=Saturday)
  Wire.write(decToBcd(dayOfMonth)); // set date (1 to 31)
  Wire.write(decToBcd(month)); // set month
  Wire.write(decToBcd(year)); // set year (0 to 99)
  Wire.endTransmission();
}
//READS time from RTC module
void readDS3231time(byte *second,
byte *minute,
byte *hour,
byte *dayOfWeek,
byte *dayOfMonth,
byte *month,
byte *year)
{
  Wire.beginTransmission(DS3231_I2C_ADDRESS);
  Wire.write(0); // set DS3231 register pointer to 00h
  Wire.endTransmission();
  Wire.requestFrom(DS3231_I2C_ADDRESS, 7);
  // request seven bytes of data from DS3231 starting from register 00h
  *second = bcdToDec(Wire.read() & 0x7f);
  *minute = bcdToDec(Wire.read());
  *hour = bcdToDec(Wire.read() & 0x3f);
  *dayOfWeek = bcdToDec(Wire.read());
  *dayOfMonth = bcdToDec(Wire.read());
  *month = bcdToDec(Wire.read());
  *year = bcdToDec(Wire.read());
}





void displayTime()
{
  byte second, minute, hour, dayOfWeek, dayOfMonth, month, year;
  
  readDS3231time(&second,&minute,&hour,&dayOfWeek,&dayOfMonth,&month,&year);

  // send it to the serial monitor
  Serial.print(hour, DEC);
  // convert the byte variable to a decimal number when displayed
  Serial.print(":");
  if (minute<10){
    Serial.print("0");
  }
  Serial.print(minute, DEC);
  Serial.print(":");
  if (second<10){
    Serial.print("0");
  }
  Serial.print(second, DEC);
  Serial.print(" ");
  Serial.print(dayOfMonth, DEC);
  Serial.print("/");
  Serial.print(month, DEC);
  Serial.print("/");
  Serial.print(year, DEC);
  Serial.print(" Day of week: ");
  switch(dayOfWeek){
  case 1:
    Serial.println("Sunday");
    break;
  case 2:
    Serial.println("Monday");
    break;
  case 3:
    Serial.println("Tuesday");
    break;
  case 4:
    Serial.println("Wednesday");
    break;
  case 5:
    Serial.println("Thursday");
    break;
  case 6:
    Serial.println("Friday");
    break;
  case 7:
    Serial.println("Saturday");
    break;
  }
  
LEDprint(second,minute,hour);
  
}

void LEDprint( byte &second,byte &minute, byte &hour)
{  
  int i;
  int BinBuffer[4];
  int* ptrH1 = dec2bin(int(hour)/10,BinBuffer);
  for (i=0;i<4;i++)
  {
    Serial.print(ptrH1[i]);
  }
  Serial.println("");
  

  int* ptrH2 = dec2bin(int(hour)%10,BinBuffer);
  for (i=0;i<4;i++)
  {
    Serial.print(ptrH2[i]);
  }
  Serial.println("");
}

//SETUP - LOOP

void setup() {
  // put your setup code here, to run once:
  Wire.begin();
  Serial.begin(9600);
  // DS3231 seconds, minutes, hours, day, date, month, year !!!! UNCOMMENT BELOW TO WRITE TO RTC MODULE !!!
  //setDS3231time(SS,MM,HH,DD,MM(MONTH),YY)
  

}

void loop() 
{
  displayTime();
  delay(1000);

  

}
