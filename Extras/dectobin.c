#include <stdio.h>
#include <stdlib.h>
int* dec2bin(int c,int *BinBuffer);

int main(int argc, char const *argv[])
{
  int BinBuffer[4];

  int i=0, a = 9;
  int* ptr = dec2bin(a,BinBuffer);
  for (i=0;i<4;i++)
  {
    printf("%d",ptr[i] );
  }

  return 0;
}


int* dec2bin(int c,int *BinBuffer)
{
   int i = 0;
  
   size_t currentsize = 0;

   for(i = 3; i >= 0; i--)
   {
     if((c & (1 << i)) != 0)
     {
       BinBuffer[currentsize++] = 1;
     }
     else
     {
       BinBuffer[currentsize++] = 0;
     }
   }
   return BinBuffer;
}
